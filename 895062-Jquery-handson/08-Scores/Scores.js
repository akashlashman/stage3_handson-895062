$(document).ready(function(){
    
    $(".txt").each(function() {

        $(this).keyup(function(){
            calculateSum();
        });
    });

});

function calculateSum() {

    var sum = 0;
    
    $(".txt").each(function() {

        
        if(!isNaN(this.value) && this.value.length!=0) {
            sum += parseFloat(this.value);
        }

    });
    
    $("#resultsum").html(sum.toFixed(2));
}
